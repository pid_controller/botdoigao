import discord
import youtube_dl
import json,os
from discord.ext import commands
from discord.utils import get
import shutil

ConfigFile="config.json"
config=json.loads(open(ConfigFile).read())

players={}
queues = {}

bot = commands.Bot(command_prefix=config["prefix"])

@bot.event
async def on_ready():
    print("Ready")


@bot.command(pass_context=True)
async def stream(ctx):
    channel=ctx.message.author.voice.channel
    voice=get(bot.voice_clients,guild=ctx.guild)
    try:
        voice = await channel.connect()
    except:
        print("Ja conectado")
    print(f"The Bot has connected to {channel}\n")
    queues.clear()
    if voice and (voice.is_paused() or voice.is_playing()):
        await voice.stop()
    else:
        print("Bot parado")
    await ctx.send("Cade a stream <@!190982156250644480>?")
    voice.play(discord.FFmpegPCMAudio("./igao/igao.mp3"), after=lambda e: print("Done"))
    voice.source = discord.PCMVolumeTransformer(voice.source)
    voice.source.volume=5

    
    
@bot.command(pass_context=True)
async def ping(ctx):
    await ctx.send(f'Pong {round(bot.latency*1000)}ms')

@bot.command(pass_context=True,aliases=['j'])
async def join(ctx):
    global voice
    channel=ctx.message.author.voice.channel
    try:
        voice = await channel.connect()
    except Exception as e:
        await ctx.send(e)
    print(f"The Bot has connected to {channel}\n")

@bot.command(pass_context=True,aliases=['l'])
async def leave(ctx):
    channel=ctx.message.author.voice.channel
    voice= get(bot.voice_clients, guild=ctx.guild)
    try:
        await voice.disconnect()
    except Exception as e:
        await ctx.send(e)
    print(f"The Bot has disconnected from {channel}\n")

@bot.command(pass_context=True,aliases=['p'])
async def play(ctx,url : str):
    def check_queue():
        queue_infile = os.path.isdir("./queue")
        if queue_infile is True:
            DIR = os.path.abspath(os.path.realpath("queue"))
            length= len(os.listdir(DIR))
            still_q = length - 1
            try:
                first_file=os.listdir(DIR)[0]
            except:
                print("Fim da fila\n")
                queues.clear()
                return
            main_location = os.path.dirname(os.path.realpath(__file__))
            song_path = os.path.abspath(os.path.realpath("queue")+"\\"+first_file)
            if length != 0 :
                print("Acabou a musica, tocando a proxima \n")
                print(f"Fila contem {still_q}")
                song_there = os.path.isfile("file.mp3")
                if song_there:
                    os.remove("file.mp3")
                shutil.move(song_path, main_location)
                for file in os.listdir("./"):
                    if file.endswith(".mp3"):
                        os.rename(file, "file.mp3")
                voice.play(discord.FFmpegPCMAudio("file.mp3"), after=lambda e: check_queue())
                voice.source = discord.PCMVolumeTransformer(voice.source)
                voice.source.volume=0.4
            else:
                queues.clear()
                return
        queues.clear()
        print("Sem musicas")
    voice=get(bot.voice_clients,guild=ctx.guild)
    if voice and voice.is_playing():
        queue_infile = os.path.isdir("./queue")
        if queue_infile is False :
            os.mkdir("queue")
        DIR=os.path.abspath(os.path.realpath("queue"))
        q_num=len(os.listdir(DIR))
        q_num=q_num+1
        add_queue = True
        while add_queue:
            if q_num in queues:
                q_num=q_num+1
            else:
                add_queue=False
                queues[q_num]=q_num
        ext=config["ext"]
        queue_path=os.path.abspath(os.path.realpath("queue") + f"\song{q_num}.%%{ext}s")
        ydl_opts = {
            'format':'bestaudio/best',
            'quiet':True,
            'outtmpl':queue_path,
            'postprocessors':[{
                'key': 'FFmpegExtractAudio',
                'preferredcodec':'mp3',
                'preferredquality':'192',
            }],
        }
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            print("Download da musica")
            ydl.download([url])
        await ctx.send("Adicionando "+str(q_num)+" a fila.")
        print("Adicionado a fila\n")
        return

    mp_file=os.path.isfile("file.mp3")
    try:
        if mp_file:
            os.remove("file.mp3")
            queues.clear()
            print("Deletado")
    except Exception as e:
        print(e)
        await ctx.send("Fudeu")
        return
    queue_infile= os.path.isdir("./queue")
    try:
        queue_folder = "./queue"
        if queue_infile is True:
            print("Deletando pasta")
            shutil.rmtree(queue_folder)
    except Exception as e:
        print(e)
    
    await ctx.send("pera disgraça")
    voice = get(bot.voice_clients, guild=ctx.guild)

    ydl_opts = {
        'format':'bestaudio/best',
        'quiet':True,
        'postprocessors':[{
            'key': 'FFmpegExtractAudio',
            'preferredcodec':'mp3',
            'preferredquality':'192',
        }],
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        print("Download da musica")
        ydl.download([url])
    
    for file in os.listdir("./"):
        if file.endswith(".mp3"):
            name=file
            os.rename(file,"file.mp3")
    nname = name.rsplit("-",2)
    await ctx.send(f"Playing: {nname[0]}")
    voice.play(discord.FFmpegPCMAudio("file.mp3"), after=lambda e: check_queue())
    voice.source = discord.PCMVolumeTransformer(voice.source)
    voice.source.volume=0.4

@bot.command(pass_context=True,aliases=['pa'])
async def pause(ctx):
    voice=get(bot.voice_clients,guild=ctx.guild)
    if voice and voice.is_playing():
        voice.pause()
        await ctx.send("pausado")
    else:
        print("falha")

@bot.command(pass_context=True,aliases=['r'])
async def resume(ctx):
    voice=get(bot.voice_clients,guild=ctx.guild)
    if voice and voice.is_paused():
        voice.resume()
        await ctx.send("Resumido")
    else:
        print("falha")

@bot.command(pass_context=True,aliases=['st'])
async def stop(ctx):
    queues.clear()
    queue_infile= os.path.isdir("./queue")
    try:
        queue_folder = "./queue"
        if queue_infile is True:
            print("Deletando pasta")
            shutil.rmtree(queue_folder)
    except Exception as e:
        print(e)
    voice=get(bot.voice_clients,guild=ctx.guild)
    if voice and (voice.is_paused() or voice.is_playing()):
        voice.stop()
        await ctx.send("Stopado")
    else:
        print("falha")


@bot.command(pass_context=True,aliases=['s'])
async def skip(ctx):
    voice=get(bot.voice_clients,guild=ctx.guild)
    if voice and (voice.is_paused() or voice.is_playing()):
        voice.stop()
    else:
        print("falha")

bot.run(config["token"])